﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfluenceSphere : MonoBehaviour
{

    public GameObject influenceSphere;
    public float distanceToSphere;
    public float H;
    public float HMax;
    public float influence;

    public AnimationCurve curve;
    public float resultCurve;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distanceToSphere = Mathf.Abs(transform.position.x - influenceSphere.transform.position.x);
        influence = Mathf.InverseLerp(influenceSphere.transform.localScale.x * 0.5f, 0, distanceToSphere);

        resultCurve = curve.Evaluate(influence);

        H = Mathf.Lerp(0, HMax, resultCurve);

        transform.position = new Vector3(transform.position.x,H,transform.position.z);
    }
}
