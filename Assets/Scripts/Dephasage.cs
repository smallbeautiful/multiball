﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dephasage : MonoBehaviour
{
    [Header("Mouse & radius")]
    public float currentInputPosition;

    public float minInputMouse;
    public float maxInputMouse;

    public float medianInputMouse;

    public float distanceToCenter;

    public float fcacul;

    public float result;

    public int amplitude = 1;
    public float frequency = 0.1f; 

    [Header("Cube")]
    public float minPosY;
    public float maxPosY;
    public float offsetY;


    // Start is called before the first frame update

    void Update()
    {
        currentInputPosition = Input.mousePosition.x;
        distanceToCenter = (currentInputPosition - medianInputMouse);
        fcacul = Mathf.Abs(distanceToCenter) / Mathf.Abs(medianInputMouse);
        //result = Mathf.Sin(fcacul - (Mathf.PI / 2));

        float x = transform.position.x;
        result = Mathf.Sin(fcacul * frequency) * amplitude;
        float z = transform.position.z;

        transform.position = new Vector3(x,result, z);
    }

}
