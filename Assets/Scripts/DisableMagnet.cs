﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMagnet : MonoBehaviour
{
    public MagnetBalls magnetPlayer;
    private void OnTriggerStay(Collider other)
    {
        if (magnetPlayer.cubeList.Contains(other))
        {
            magnetPlayer.cubeList.Remove(other);
        }
    }
}
