﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowVelocity : MonoBehaviour
{
    public Rigidbody target;
    private Rigidbody mybody;
    // Start is called before the first frame update
    void Start()
    {
        mybody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        mybody.velocity = mybody.velocity + (target.velocity / 30);
    }
}
