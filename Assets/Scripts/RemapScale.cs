﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemapScale : MonoBehaviour
{
    public float minInputMouse;
    public float maxInputMouse;
    public float minPosY;
    public float maxPosY;
    public float startPosY;
    public float currentPosY;


    public float minPosZ;
    public float maxPosZ;
    public float startPosZ;
    public float currentPosZ;

    public float currentInputPosition;

    [Header("Part2")]
    public float diffY;
    public float diffZ;
    public float nextPosY1;
    public float nextPosZ1;
    public float nextPosY2;
    public float nextPosZ2;

    private void Start()
    {
        startPosY = transform.localPosition.y;
        startPosZ = transform.localPosition.z;
        diffY = startPosY - ((minPosY + maxPosY) / 2);
        diffZ = startPosZ - ((minPosZ + maxPosZ) / 2);
    }
    // Update is called once per frame
    void Update()
    {
        currentInputPosition = Input.mousePosition.x;
        if (currentInputPosition >= minInputMouse && currentInputPosition <= maxInputMouse)
        {
            currentPosY = currentInputPosition.Remap(minInputMouse, maxInputMouse, minPosY, maxPosY);
            currentPosZ = currentInputPosition.Remap(minInputMouse, maxInputMouse, minPosZ, maxPosZ);
            nextPosY1 = currentPosY + diffY;
            nextPosZ1 = currentPosZ + diffZ;
            nextPosY2 = currentPosY - diffY;
            nextPosZ2 = currentPosZ - diffZ;

            //if (nextPosY1 > maxPosY || nextPosZ1 > maxPosZ)
            //{
            //    Debug.Log("Problème de trop de Y pour : " + gameObject.name);
            //    float diffPosY = nextPosY1 - maxPosY;
            //    nextPosY1 = maxPosY - diffPosY;
            //    Debug.Log("Problème de trop de Z pour : " + gameObject.name);
            //    float diffPosZ = nextPosZ1 - maxPosZ;
            //    nextPosZ1 = maxPosZ - diffPosZ;
            //    transform.localPosition = new Vector3(transform.localPosition.x, nextPosY1, nextPosZ1);
            //}
            //else if (nextPosY2 < minPosY || nextPosZ2 < minPosZ)
            //{
            //    Debug.Log("Problème de pas assez de Y pour : " + gameObject.name);
            //    float diffPosY = nextPosY2 - maxPosY;
            //    nextPosY2 = maxPosY + diffPosY;
            //    Debug.Log("Problème de pas assez de Z pour : " + gameObject.name);
            //    float diffPosZ = nextPosZ2 - maxPosZ;
            //    nextPosZ2 = maxPosZ + diffPosZ;
            //    transform.localPosition = new Vector3(transform.localPosition.x, nextPosY2, nextPosZ2);
            //}
            //else
            //{
                Debug.Log("else pour : " + gameObject.name);
                transform.localPosition = new Vector3(transform.localPosition.x, currentPosY, currentPosZ);
            //}


          
        }
    }
}
