﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCursor : MonoBehaviour
{
    public float currentInputPosition;
    public float minInputMouse;
    public float maxInputMouse;
    public float minSphereX;
    public float maxSphereX;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            currentInputPosition = Input.mousePosition.x;
            currentInputPosition = currentInputPosition.Remap(minInputMouse, maxInputMouse, minSphereX, maxSphereX);
            transform.position = new Vector3(currentInputPosition, transform.position.y, transform.position.z);
        }
    }
}