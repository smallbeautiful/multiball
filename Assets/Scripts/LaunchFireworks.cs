﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchFireworks : MonoBehaviour
{
    public List<ParticleSystem> particleSystems = new List<ParticleSystem>();
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            foreach (ParticleSystem part in particleSystems)
            {
                part.Play();
            }
        }
    }
}
