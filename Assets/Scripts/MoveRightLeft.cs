﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRightLeft : MonoBehaviour
{
    public Rigidbody body;
    public float horizontalSpeed;

    // Start is called before the first frame update
    void Start()
    {
        if (!body)
        {
            body = GetComponent<Rigidbody>();
        }   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.Mouse0))
        {
            float h = horizontalSpeed * Input.GetAxis("Mouse X");
            body.velocity = new Vector3(h, 0, 0);
        }
        else
        {
            body.velocity = new Vector3(0, 0, 0);

        }
    }
}
