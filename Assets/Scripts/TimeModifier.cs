﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeModifier : MonoBehaviour
{
    public float changeTimeScale;
    public bool endChange;
    public float timerWait;
    public float scaleValue;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(UpTimeScale());
        }
    }

    private IEnumerator UpTimeScale()
    {
        endChange = true;
        while (scaleValue <= changeTimeScale -0.1f)
        {
            scaleValue = Mathf.Lerp(scaleValue, changeTimeScale, 0.1f);
            Time.timeScale = scaleValue;
            yield return null;
        }
        scaleValue = changeTimeScale;
        Time.timeScale = changeTimeScale;
        yield return new WaitForSecondsRealtime(timerWait);
        endChange = false;
        Time.timeScale = 1f;
        yield return null;
    }
}
