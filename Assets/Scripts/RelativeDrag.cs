﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelativeDrag : MonoBehaviour
{
    public Rigidbody collectableRigidbody;
    public Rigidbody targetRigidbody;
    public float stayCloseDampingBegin;
    public float stayCloseDampingEnd;
    public AnimationCurve stayCloseCurve;
    public float stayCloseInnerRadius;
    public float stayCloseOuterRadius;
    public bool debug;

    [Header("Debug")]
    public Vector3 debug_relativeVelocity;
    public Vector3 debug_relativeDampedVelocity;
    public float debug_relativeDamping;
    public Vector3 debug_velocity;
    public Vector3 debug_dampedVelocity;
    public float debug_damping;
    private bool debug_manageGoingAway;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            targetRigidbody =  other.GetComponent<Rigidbody>();
        }
    }

    private void Start()
    {
        collectableRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (!targetRigidbody)
        {
            return;
        }
        debug_velocity = collectableRigidbody.velocity;
        // check direction and damp velocity if necessary
        collectableRigidbody.velocity = StayCloseDamping(collectableRigidbody.position, collectableRigidbody.velocity, targetRigidbody.position, targetRigidbody.velocity);
#if UNITY_EDITOR
        debug_dampedVelocity = collectableRigidbody.velocity;
        debug_damping = debug_dampedVelocity.magnitude / debug_velocity.magnitude;
#endif
    }

    private Vector3 StayCloseDamping(Vector3 elementPosition, Vector3 elementVelocity, Vector3 referentialPosition, Vector3 referentialVelocity)
    {
        Vector3 relativeElementVelocity = elementVelocity - referentialVelocity;

        Vector3 relativeElementPosition = elementPosition - referentialPosition;

        Vector3 relativeElementDampedVelocity = StayCloseDampingRelative(relativeElementPosition, relativeElementVelocity);

        Vector3 elementDampedVelocity = relativeElementDampedVelocity + referentialVelocity;

        return elementDampedVelocity;
    }

    private Vector3 StayCloseDampingRelative(Vector3 position, Vector3 velocity)
    {

#if UNITY_EDITOR
        debug_relativeVelocity = velocity;
#endif
        float distanceFromCenter = position.magnitude;

        float innerRadius = stayCloseInnerRadius;
        float outerRadius = stayCloseOuterRadius;


        float diffOuterInner = outerRadius - innerRadius;

        float dampingPercent = 0;

        if (distanceFromCenter <= innerRadius)
        {
            dampingPercent = 0;
        }
        else if (distanceFromCenter >= outerRadius)
        {
            dampingPercent = 1;
        }
        //else
        //{
        // interpolate 
        //float radiusPercent = Mathf.InverseLerp(innerRadius, outerRadius, distanceFromCenter);
        //dampingPercent = stayCloseCurve.Evaluate(radiusPercent);

        //}

        float damping = Mathf.Lerp(stayCloseDampingBegin, stayCloseDampingEnd, dampingPercent);
        bool isGoingAway = Vector3.Dot(position, velocity) > 0;
        
        if (debug_manageGoingAway && !isGoingAway)
        {
            return velocity;
        }

        // 
        velocity *= Mathf.Clamp01(1.0f - damping);
#if UNITY_EDITOR
        debug_relativeDampedVelocity = velocity;

        debug_relativeDamping = debug_relativeDampedVelocity.magnitude / debug_relativeVelocity.magnitude;
#endif
        return velocity;
    }


}