﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetBalls : MonoBehaviour
{
    public float magnetForce = 15.0f;
    public bool attract = true;
    public float innerRadius = 2.0f;
    public float outerRadius = 5.0f;
    public Rigidbody targetRigidbody;

    public List<Collider> cubeList = new List<Collider>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            cubeList.Add(other);
            other.GetComponent<MoveRightLeft>().enabled = true;
        }
    }

    void Update()
    {
        AssignObjects();
    }

    private void AssignObjects()
    {
        foreach (Collider col in cubeList)
        {
            if (col.gameObject)
                if (col.gameObject.GetComponent<Rigidbody>())
                    attractOrRepel(col);
        }
    }

    void attractOrRepel(Collider col)
    {
        if (Vector3.Distance(transform.position, col.transform.position) > innerRadius)
        {
            if (col.CompareTag("Player"))
            {
                if (Vector3.Distance(transform.position, col.transform.position) > innerRadius)
                {
                    //Apply force in direction of magnet center
                    if (attract)
                    {
                        float dynamicDistance = Mathf.Abs((Vector3.Distance(transform.position, col.transform.position)) - (outerRadius + (innerRadius * 2)));
                        float multiplier = dynamicDistance / outerRadius;

                        col.GetComponent<Rigidbody>().AddForce((magnetForce * (transform.position - col.transform.position).normalized) * multiplier, ForceMode.Force);
                    }
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, innerRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, outerRadius);

        Gizmos.DrawIcon(transform.position, "sptk_magnet.png", true);
    }
}
