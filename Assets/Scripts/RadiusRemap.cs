﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiusRemap : MonoBehaviour
{
    [Header("Mouse & radius")]
    public float currentInputPosition;

    public float minInputMouse;
    public float maxInputMouse;

    public float medianInputMouse;

    public float distanceToCenter;

    public float fcacul;

    [Header("Cube")]
    public float minPosY;
    public float maxPosY;
    public float startPosY;
    public float currentPosY;


    public float minPosZ;
    public float maxPosZ;
    public float startPosZ;
    public float currentPosZ;

    [Header("Move")]
    public float influenceZ;
    public float influenceY;
    public float hauteur;
    public AnimationCurve curve;
    public float evaluate;

    void Start()
    {
        startPosY = transform.localPosition.y;
        startPosZ = transform.localPosition.z;
    }

    // Update is called once per frame
    void Update()
    {
        currentInputPosition = Input.mousePosition.x;
        distanceToCenter = (currentInputPosition - medianInputMouse);
        fcacul = Mathf.Abs(distanceToCenter) / Mathf.Abs(medianInputMouse);

        //influenceZ = (fcacul * maxPosY);
        influenceY = Mathf.InverseLerp(minPosY,maxPosY, fcacul);
 
        evaluate = curve.Evaluate(fcacul);
        hauteur = Mathf.Lerp(minPosY, maxPosY, evaluate);


        transform.position = new Vector3(transform.position.x, (hauteur / 2), transform.position.z);
    }
}
